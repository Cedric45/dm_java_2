import java.util.*;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if(liste.size()==0){return null;}

        Integer res = liste.get(0);

        for(Integer elem:liste)
            if(elem<=res)
                res=elem;

        return res;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if(liste.size()==0){return true;}

        for(T elem:liste){
            if(elem.compareTo(valeur)<=0)
                return false;
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        if(liste1.size() == 0 && liste2.size() == 0){return new ArrayList<T>();}

        if( liste1.get(liste1.size()-1).compareTo(liste2.get(0))<0 ){return new ArrayList<T>();}

        List<T> res = new ArrayList<>();

        for(T elem1:liste1){
            if(liste2.contains(elem1)){
                if(!res.contains(elem1))
                    res.add(elem1);
            }
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> liste = new ArrayList<>();

        if(texte.length() == 0){return liste;}

        String mot = "";
        char lettre = ' ';

        for(int i=0;i<texte.length();i++){
            lettre = texte.charAt(i);
            if(lettre == ' '){
                if(! (mot == ""))
                    liste.add(mot);
                mot="";
            }
            else
                mot+=lettre;
        }
        if(! (mot == ""))
            liste.add(mot);

        return liste;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if(texte.equals("")){return null;}

        Map<String, Integer> dicMots = new HashMap<>();
        Integer valeur=0;
        List<String> listeMots = decoupe(texte);

        for(String mot:listeMots){
            if(! dicMots.keySet().contains(mot))
                dicMots.put(mot,1);
            else{
                valeur = dicMots.get(mot);
                dicMots.remove(mot);
                dicMots.put(mot,valeur+1);
            }
        }

        List<String> listemots = new ArrayList<>(dicMots.keySet());
        String motMax = listemots.get(0);
        int max = dicMots.get(motMax);
        int curVal = 0;


        for(String mot:dicMots.keySet()){
            curVal = dicMots.get(mot);
            if(curVal>max){
                motMax = mot;
                max = curVal;
            }
            else
                if(curVal == max){
                    if(mot.compareTo(motMax)<0)
                        motMax=mot;
                }
        }
        return motMax;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if(chaine.equals("")){return true;}

        if( (chaine.charAt(chaine.length()-1) != chaine.charAt(chaine.length()-2) ) && chaine.charAt(chaine.length()-2) != ']')
            return false;

        int cpt1=0;
        int cpt2=0;
        for(int i=0;i<chaine.length();i++){
            char lettre = chaine.charAt(i);
            if(lettre == '(')
                cpt1+=1;
            if(lettre == ')')
                cpt2+=1;
        }
        return cpt1 == cpt2;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        if(! bienParenthesee(chaine)){return false;}

        if(chaine.equals("")){return true;}

        if( (chaine.charAt(chaine.length()-1) != chaine.charAt(chaine.length()-2) ) && chaine.charAt(chaine.length()-2) != ')')
            return false;

        int cpt1=0;
        int cpt2=0;
        for(int i=0;i<chaine.length();i++){
            char lettre = chaine.charAt(i);
            if(lettre == '[')
                cpt1+=1;
            if(lettre == ']')
                cpt2+=1;
        }
        return cpt1 == cpt2;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int bas = 0;
        int haut = liste.size()-1;
        int milieu = 0;
        while(bas<haut){
            milieu=(bas+haut)/2;
            if(liste.get(milieu)<valeur)
                bas=milieu+1;
            else
                haut = milieu;
        }
        return bas<liste.size() && valeur == liste.get(bas);
    }
}
